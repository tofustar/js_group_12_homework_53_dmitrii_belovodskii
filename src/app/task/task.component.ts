import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  @Input() taskText = '';
  @Output() delete = new EventEmitter();
  @Output() textChange = new EventEmitter<string>();
  onFocus = false;

  onClickDelete() {
    this.delete.emit();
  }

  getFocusClass() {
    if (this.onFocus) return 'active';
    else return '';
  }

  onTaskInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.textChange.emit(target.value);
  }



}
