import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  taskText = '';
  tasks = [
    {taskText: 'Buy milk'},
    {taskText: 'Wash car'}
  ];

  addTask(event: Event){
    event.preventDefault();
    this.tasks.push({
      taskText: this.taskText
    });
    this.taskText = '';
  }

  onDeleteTask(index: number) {
    this.tasks.splice(index, 1);
  }

  changeText(index :number, newText: string){
    this.tasks[index].taskText = newText;
  }

}
